use combine::{parser::range::range, Parser, RangeStream};
use combine_macros::parser;

#[parser]
fn parse_hello<'a, I>() -> impl Parser<Input = I, Output = &'a str>
where
    I: RangeStream<Item = char, Range = &'a str>,
{
    range("hello")
}

#[parser(A)]
fn parse_hello_custom<'a, A>() -> impl Parser<Input = A, Output = &'a str>
where
    A: RangeStream<Item = char, Range = &'a str>,
{
    range("hello")
}

#[test]
fn works() {
    let h = parse_hello().parse("hello").map(|p| p.0).unwrap();
    assert_eq!(h, "hello");
    let hc = parse_hello_custom().parse("hello").map(|p| p.0).unwrap();
    assert_eq!(hc, "hello");
}
