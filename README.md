# combine-macros

## Usage

```toml
[dependencies]
combine-macros = { git = "https://gitlab.com/samgreen/combine-macros" }
```

### `parser`

Adds a `combine::ParseError` where clause to a parser function.

```rust
#[parser]
fn parse_hello<'a, I>() -> impl Parser<Input = I, Output = &'a str>
where
    I: RangeStream<Item = char, Range = &'a str>,
{
    range("hello")
}
```

Assumes input type parameter named `I`. If a different name is used, it must be specified as an
argument to the macro:

```rust
#[parser(A)]
fn parse_hello<'a, A>() -> impl Parser<Input = A, Output = &'a str>
where
    A: RangeStream<Item = char, Range = &'a str>,
{
    range("hello")
}
```

