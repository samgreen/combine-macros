extern crate proc_macro;

use proc_macro::TokenStream;
use proc_macro2::Ident;
use quote::{quote, ToTokens};
use syn::{parse_macro_input, ItemFn};

/// Adds a `combine::ParseError` where clause to a parser function.
///
/// ```ignore
/// #[parser]
/// fn parse_hello<'a, I>() -> impl Parser<Input = I, Output = &'a str>
/// where
///     I: RangeStream<Item = char, Range = &'a str>,
/// {
///     range("hello")
/// }
/// ```
///
/// Assumes input type parameter named `I`. If a different name is used, it must be specified as an
/// argument to the macro:
///
/// ```ignore
/// #[parser(A)]
/// fn parse_hello<'a, A>() -> impl Parser<Input = A, Output = &'a str>
/// where
///     A: RangeStream<Item = char, Range = &'a str>,
/// {
///     range("hello")
/// }
/// ```
#[proc_macro_attribute]
pub fn parser(args: TokenStream, input: TokenStream) -> TokenStream {
    let mut input: ItemFn = parse_macro_input!(input as ItemFn);
    let generic: Ident = syn::parse(args).unwrap_or_else(|_| syn::parse_quote!(I));

    let where_clause = input.decl.generics.make_where_clause();
    where_clause.predicates.push(syn::parse_quote!(
        #generic::Error: combine::ParseError<#generic::Item, #generic::Range, #generic::Position>
    ));

    input.into_token_stream().into()
}
